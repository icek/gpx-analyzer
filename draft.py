# -*- coding: utf-8 -*-
'''Copyright (c) 2013, Łukasz Gemborowski
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.'''

import math
from datetime import datetime
import xml.etree.ElementTree as ET

""" Represents part of the track between two GPS measurements. """
class Segment:
    def __init__(self):
        self.begin = (None, None)
        self.end = (None, None)
        self.beginTime = None
        self.endTime = None
        self.beginElevation = None
        self.endElevation = None

    """
    Initialize Segment object with all params.

    begin/end - floats tuple in format: (lat, lon)
    time - string in gpx format
    elevation - float, value in meters
    """
    def __init__(self, begin, end, startTime, endTime, startEle, endEle):
        self.begin = begin
        self.end = end
        self.beginTime = datetime.strptime(startTime, '%Y-%m-%dT%H:%M:%SZ')
        self.endTime = datetime.strptime(endTime, '%Y-%m-%dT%H:%M:%SZ')
        self.beginElevation = startEle
        self.endElevation = endEle

    def setBegin(self, location):
        self.begin = location

    def setEnd(self, location):
        self.end = location

    def setBeginTime(self, time):
        self.beginTime = time

    def setEndTime(self, time):
        self.endTime = time

    def setBeginElevation(self, elevation):
        self.beginElevation = elevation

    def setEndElevation(self, elevation):
        self.endElevation = elevation

    def getDistance(self):
        # ref: http://www.platoscave.net/blog/2009/oct/5/calculate-distance-latitude-longitude-python/
        lat1, lon1 = self.begin
        lat2, lon2 = self.end
        radius = 6371 # km

        dlat = math.radians(lat2-lat1)
        dlon = math.radians(lon2-lon1)
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
            * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = radius * c
        return d

    def getSeconds(self):
        return (self.endTime - self.beginTime).seconds
        
"""
Parse <trkpt> node into 4 element tuple (lat, lon, elevation, time)
"""
def parse_point(point):
    lat = point.get('lat')
    lon = point.get('lon')

    time_tag = point.find('{http://www.topografix.com/GPX/1/1}time')
    time = time_tag.text

    elevation_tag = point.find('{http://www.topografix.com/GPX/1/1}ele')
    ele = None
    
    if elevation_tag != None:
        ele = float(elevation_tag.text)

    return (float(lat), float(lon), ele, time)

"""
Parse .gpx file into list of Segment objects
"""
def load_segments(filename):
    tree = ET.parse(filename)
    root = tree.getroot()

    track = []
    segments = []

    # get all <trkpt> tags and parse them
    for p in root.iter('{http://www.topografix.com/GPX/1/1}trkpt'):
        v =  parse_point(p)
        track.append(v)

    elevation = None
    # find first elevation
    for t in track:
        if t[2] != None:
            elevation = t[2]/1000.0
            break

    # extract all segments
    for i in range(len(track)-1):
        p1 = track[i]
        p2 = track[i + 1]
        new_elevation = elevation

        if p2[2] != None:
            # new elevation!
            new_elevation = p2[2]/1000.0

        s = Segment(p1[:-2], p2[:-2], p1[3], p2[3], elevation, new_elevation)
        segments.append(s)
        elevation = new_elevation

    return segments

"""
Some simple usage of Segments list
"""
def show_stats(segments):
    total = 0.0
    time = 0.0
    for s in segments:
        time += s.getSeconds()
        total += s.getDistance()

    print str(total) + 'km in ' + str(time/60) + 'min, avg speed: ' + str(total/(time/(60*60))) + 'km/h'
